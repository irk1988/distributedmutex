/*
 * Program to implement a node in a fully-connected TCP network + Implementation of Ricart-Agarwala mutual exclusion algorithm
 * Author: Philip Karunakaran, Immanuel Rajkumar
 * ixp140230
 */

#include<iostream>
#include<fstream>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<unistd.h>
#include<thread>
#include<mutex>
#include<chrono>
#include<ctime>
#include<cstdlib>

#define PORT_NO 9877 //Nodes will have their port number set as PORT_NO + node_id (Node id: 0-9)

#define NODES 2 //Number of nodes present in the system - In our test system - 10

#define UNIT_TIME 100000 // Unit physical time in microsec - default is 100 msec

#define PHASE1 20 // No of times a node enters critical section in phase 1 and 2 - variable for testing purpose
#define PHASE2 20

using namespace std;

/*Lamport Clock*/
class LClock
{
public:
    int clock;
    int d;
    mutex clock_mu;

    LClock() {
        clock=0;
        d = 1;
    }

    void internalEvent() {
        clock_mu.lock();
        clock +=d;
        clock_mu.unlock();
    }

    void externalEvent( int extTime ) {
        clock_mu.lock();
        extTime > clock ? clock=extTime : clock+=d;
        clock_mu.unlock();
    }
};

/* Shared Messages List */
class Messages
{
private:
    vector<string> msgs;
    mutex msg_mu;
public:
    void addMsg( string m ) {
        msg_mu.lock();
        msgs.push_back(m);
        msg_mu.unlock();
    }

    void printAllMsgs(int nodeId, bool sent, ofstream& out) {
        msg_mu.lock();
        vector<string>::iterator k, BEGIN=msgs.begin(), END=msgs.end();
        for( k=BEGIN; k!=END; ++k ) {
            string s = (*k);
            if( sent )
                out<<"MSG:"<<s<<" FROM: Node"<<s.at(0)<<" TO: Node"<<s.at(2)<<" TYPE:";
            else
                out<<"MSG:"<<s<<" FROM: Node"<<s.at(2)<<" TO: Node"<<s.at(0)<<" TYPE:";
            if( s.at(4) == '1' ) out<<"SETUP";
            else if( s.at(4) == '2' ) out<<"REQ";
            else if( s.at(4) == '3' ) out<<"REP";
            else if( s.at(4) == '4' ) out<<"TERM";
            out<<endl;
        }
        msg_mu.unlock();
    }
};

/* Simple structure to store a node address */
struct Addr {
    int nodeId;
    int port;
    string ip;
    string hostname;
};

/* Node class - Properties and behaviour of a node in a fully connected TCP nw */
class Node
{

private:
    int nodeId;
    int listeningPortNo;
    int listenfd; // sock fd of the listening port
    string ip;
    char outfile[2];
    ofstream out;

    vector<int> sockfds; // Array to store the connection descriptors of communication channels
    vector<Addr> peers;

    LClock lclock;
    Messages received_msgs, sent_msgs;
    mutex c_mutex; //Common mutex for accessing shared variables in the Node

    //Algorithm specific data
    int outstanding_reply_count;
    int reply_recvd;
    bool requesting_critical_section;
    vector<bool> reply_deferred;
    vector<bool> reply_required;
    int termination_count;

public:
    /* Constructor to initialize all required variables */
    Node( int id ) {
        nodeId = id;
        listeningPortNo = PORT_NO + id;
        for( int i=0; i< NODES; i++ ) {
            sockfds.push_back(0);
            reply_deferred.push_back(false);
            reply_required.push_back(true);
        }
        requesting_critical_section = false;
        outstanding_reply_count = NODES;
        reply_recvd = 0;
        termination_count=0;
        /* Opening file for writing output */
        sprintf(outfile, "%d", nodeId);
        out.open(outfile);
        feedPeerInfo();
    }

    /* Store the Peer address + port info to the list peers - Host info file : "hosts" */
    int feedPeerInfo() {
        ifstream ifile("hosts");
        string s;
        int i=0;
        if( ifile.is_open() ) {
            ifile.seekg(0, ios::beg);
            while( ifile >> s ) {
                Addr peer;
                peer.nodeId = i;
                peer.hostname = s;
                peer.port = PORT_NO + i;
                i++;
                peers.push_back(peer);
            }
        }
        ifile.close();
        return 0;
    }

    /* Invokes the Listener thread creation routine.
     * Establish connection to all the nodes with id lesser than itself by invoking the connectPeers module
     */
    int initConnections() {
        thread listenerThread(&Node::initListener, this);

        for( int i=0; i<nodeId; i++ )
            connectPeer(peers[i]);

        thread testBC(&Node::startCriticalSection, this);
        testBC.join();

        listenerThread.join();
        return 0;
    }

    /* Open socket for listening and spawn new thread for it */
    int initListener( ) {
        listenfd = socket( AF_INET, SOCK_STREAM, 0 );
        if( listenfd < 0 ) {
            out<<"ERROR OPENING SOCKET"<<endl;
            return 0;
        }

        struct sockaddr_in serv_addr, client_addr;
        socklen_t caddr_len;

        memset( &serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); //Accept from any interface
        serv_addr.sin_port = htons(listeningPortNo);

        bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

        listen(listenfd, 10);
        caddr_len = sizeof(client_addr);

        int confd = 0;
        while(1) {
            confd = accept(listenfd, (struct sockaddr *)&client_addr, &caddr_len );
            thread t(&Node::handleClient, this, confd);
            t.detach();
        }

        return 0;
    }

    /* handle each connection spawned as thread- wait for msgs and invoke processMsg when received */
    void handleClient( int confd ) {
        char buff[255];
        memset(buff, 0, sizeof(buff));
        while( read( confd, buff, 255 ) ) {
            processMsg(buff, confd);
        }
    }

    /* Establish a TCP connection between this node and Peer sent as parameter */
    int connectPeer( Addr p ) {
        int sockfd;
        struct sockaddr_in servaddr;
        struct hostent * server;

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if( sockfd < 0 ) {
            out<<"ERROR OPENING SOCKET"<<endl;
            return 0;
        }

        server = gethostbyname(p.hostname.c_str());

        memset( &servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        memcpy(&servaddr.sin_addr, server->h_addr_list[0], sizeof(server->h_addr_list[0]));
        servaddr.sin_port = htons(p.port); //get it from arg

        connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
        sockfds[p.nodeId] = sockfd;
        thread t(&Node::handleClient, this, sockfd);
        t.detach();
        //Send a setup test msg, so the end connection can identify the sender
        char testMsg[255];
        memset(testMsg, 255, 0 );
        sprintf(testMsg, "%d-%d-1-%d-", nodeId, p.nodeId, lclock.clock);
        send(sockfd, testMsg, 255, 0);
        sent_msgs.addMsg(testMsg);
        out<<"NODE"<<nodeId<<"- CONNECTION ESTD BETWEEN NODE"<<nodeId<<" AND NODE"<<p.nodeId<<endl;

        return 0;
    }

    /* Start entering critical section phases */
    int startCriticalSection() {
        /* Wait time to ensure all the connections are established before issuing requests */
        sleep(8-nodeId);

        //random wait time generator variables
        /* c++11 support was not there in CS machines and code has been revoked
        std::random_device rd;
               std::mt19937 gen(rd());
               std::uniform_int_distribution<> dis(5,10);
        */

        //Phase - 1 // Need to change it to 20 times
        for( int i=0; i<PHASE1; i++ ) {
            usleep(UNIT_TIME* (5 + (rand() % (int)(10 - 5 + 1)))); //Sleep
            requestCriticalSection(i+1);
        }
        //Phase - 2 // Need to change the count to 20
        for( int i=0; i<PHASE2; i++ ) {
            if( nodeId%2 == 0 )
                usleep(UNIT_TIME* ( 45 + (rand() % (int)(50 - 45 + 1 )))); //Sleep for 45-50 time units
            else
                usleep(UNIT_TIME* (5 + (rand() % (int)(10 - 5 + 1)))); //Sleep
            requestCriticalSection(i+21);
        }

        /* Send deferred msgs to any processes waiting */
        requesting_critical_section = false;
        for( int i=0; i<NODES; i++ ) {
            if( i != nodeId ) { //send to all except itself
                if( reply_deferred[i]  ) {
                    char msg[255];
                    memset( msg, 0, sizeof(msg) );
                    sprintf(msg, "%d-%d-3-%d-",nodeId, i, lclock.clock);
                    send( sockfds[i], msg, 255, 0);
                    sent_msgs.addMsg(msg);
                    lclock.internalEvent();
                }
            }
        }
        displayConversation();
        terminate();
        return 0;
    }

    /* Routine called for computation termination */
    int terminate() {
        if( nodeId == 0 ) {
            //Termination computation node - Waits for all the term msgs from nodes and then issues shutdown msg */
            while( termination_count < (NODES-1) );
            for( int i=1; i<NODES; i++ ) {
                char msg[255];
                memset( msg, 0, sizeof(msg) );
                sprintf(msg, "%d-%d-5-%d-",nodeId, i, lclock.clock);
                lclock.internalEvent();
                send( sockfds[i], msg, 255, 0);
                sent_msgs.addMsg(msg);
            }
            sleep(4);
            exit(0);
        } else {
            /* If not the termination computation node, send msg to the term computation node - in this case node0 */
            char msg[255];
            memset( msg, 0, sizeof(msg) );
            sprintf(msg, "%d-0-4-%d-",nodeId, lclock.clock);
            lclock.internalEvent();
            send( sockfds[0], msg, 255, 0);
            sent_msgs.addMsg(msg);
            out<<"TERMINATE CALLED BY NODE"<<nodeId<<endl;
            while(1);
        }
        return 0;
    }


    int requestCriticalSection(int count) {
        c_mutex.lock();
        requesting_critical_section = true;
        c_mutex.unlock();
        char msg[255];
        /* Send request to nodes whose consent is required - NOTE: after optimizaion, this is not always all nodes */
        for( int i=0; i<NODES; i++ ) {
            if( i != nodeId ) { //send to all except itself
                if( reply_required[i] ) {
                    memset( msg, 0, sizeof(msg) );
                    sprintf(msg, "%d-%d-2-%d-",nodeId, i, lclock.clock);
                    send( sockfds[i], msg, 255, 0);
                    sent_msgs.addMsg(msg);
                    lclock.internalEvent();
                }
            }
        }
        /* Check how many replies are needed to enter a critical section */
        c_mutex.lock();
        outstanding_reply_count = 0;
        vector<bool>::iterator w, BEGIN=reply_required.begin(), END=reply_required.end();
        for( w=BEGIN; w!=END; ++w ) {
            if(*w) outstanding_reply_count++;
        }
        c_mutex.unlock();

        /* Wait till all the required number of replies are received */
        while( reply_recvd < outstanding_reply_count-1  ); //Except for its own reply

        reply_recvd = 0;
        /* Critical Section Begin */
        out<<"NODE"<<nodeId<<"- ENTERING CRITICAL SECTION. VISIT_COUNT: "<<count<<endl;
        std::chrono::time_point<std::chrono::system_clock> end;
        end = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);
        out<<std::ctime(&end_time)<<endl;
        usleep(UNIT_TIME * 3);
        //cout<<"NODE"<<nodeId<<"- EXITING CRITICAL SECTION. VISIT_COUNT: "<<count<<endl;
        /* Critical Section End */

        /* Send deferred replies */
        requesting_critical_section = false;
        for( int i=0; i<NODES; i++ ) {
            if( i != nodeId ) { //send to all except itself
                if( reply_deferred[i] ) {
                    memset( msg, 0, sizeof(msg) );
                    sprintf(msg, "%d-%d-2-%d-",nodeId, i, lclock.clock);
                    send( sockfds[i], msg, 255, 0);
                    sent_msgs.addMsg(msg);
                    lclock.internalEvent();
                }
            }
        }
        return 0;
    }

    /* Routine to process the incoming messages  - Messages are formulated based on format mentioned below */
    int processMsg( char * m , int confd) {
        //MSG format : <node_id-1char-TO>-<node_id-1char-FROM>-<msgType-1char>-<clock-remaining>-
        received_msgs.addMsg(m);
        lclock.internalEvent();
        int from=0;
        from = m[0]-'0';

        char stime[10];
        int i;
        for( i=6; m[i]!='-'; i++ )
            stime[i-6] = m[i];
        stime[i] = '\0';
        int m_time = atoi(stime);

        if( m[4] == '1' ) {
            //Setup msg
            if( sockfds[from] == 0 )
                sockfds[from] = confd;
            out<<"NODE"<<nodeId<<"- CONNECTION ESTD BETWEEN NODE"<<nodeId<<" AND NODE"<<from<<endl;
        } else if( m[4] == '2' ) {
            //Request msg
            bool defer_it = false;
            c_mutex.lock();
            defer_it = ( requesting_critical_section && ((m_time > lclock.clock) || (m_time==lclock.clock && from>nodeId)) );
            c_mutex.unlock();
            if( defer_it == true ) {
                reply_deferred[from]=true;
            } else {
                char msg[255];
                memset( msg, 0, sizeof(msg) );
                sprintf(msg, "%d-%d-3-%d-",nodeId, from, lclock.clock);
                lclock.internalEvent();
                send( sockfds[from], msg, 255, 0);
                sent_msgs.addMsg(msg);
                reply_required[from] = true;
            }

        } else if( m[4] == '3' ) {
            //Reply msg
            //out<<"NODE"<<nodeId<<"- REPLY MSG FROM : "<<from<<endl;
            c_mutex.lock();
            reply_required[from]=false;
            reply_recvd++;
            c_mutex.unlock();
        } else if( m[4] == '4' ) {
            //out<<"NODE"<<nodeId<<"- COMPUTATION TERMINATION MSG FROM : "<<from<<" MSG : "<<m<<endl;
            termination_count++;
        } else if( m[4] == '5' ) {
            //Shutdown message
            out<<"NODE"<<nodeId<<"- TERMINATE RECVD"<<endl;
            out.close();
            exit(0);
        }

        lclock.externalEvent(m_time);
        return 0;
    }

    /* Routine to display all the sent and received messages */
    int displayConversation() {
        sleep(5);
        out<<"\nPRINTING ALL MSGS RECIEVED BY NODE :"<<nodeId<<endl;
        received_msgs.printAllMsgs(nodeId, false, out);
        out<<"\nPRINTING ALL MSGS SENT BY NODE :"<<nodeId<<endl;
        sent_msgs.printAllMsgs(nodeId, true, out);
        return 0;
    }
};


/* Main Program - Argument - single digit NodeId */
int main(int argc, char * argv[])
{
    int id = 0;
    if( argc > 1 )
        id = atoi(argv[1]);
    cout<<"NODE NAME: "<<id<<endl;
    Node node(id);
    node.initConnections();
    node.startCriticalSection();
    return 0;
}
